window.addEventListener('DOMContentLoaded', function() {
    document.querySelector('.main-select__main-item').addEventListener('keyup', (event) => {
        if ( event.key == 'Enter') {
            document.querySelector('.main-select__other-item').classList.toggle('open')
            document.querySelector('.main-select__main-item').classList.toggle('main-open')
        }
    })

    document.querySelectorAll('.main-select__item').forEach(function(t) {
        t.addEventListener('click', function (event) {
            const target = event.target
            document.querySelector('.main-select__main-item').innerHTML = target.innerHTML
        })
        t.addEventListener('keyup', (event) => {
            const target = event.target
            if ( event.key == 'Enter') {
                document.querySelector('.main-select__main-item').innerHTML = target.innerHTML
            }
        })
    })

    document.querySelector('.main-select__main-item').addEventListener('click', function () {
        document.querySelector('.main-select__other-item').classList.toggle('open')
        document.querySelector('.main-select__main-item').classList.toggle('main-open')
    
    })


    document.querySelector('.main-select__item-search').addEventListener('input', function() {
        document.querySelectorAll('.main-select__item').forEach(function(item) {
            const input = document.querySelector('.main-select__item-search').value.toUpperCase()
            if (item.innerHTML.toUpperCase().indexOf(input) > -1) {
                item.style.display = "";
            } else {
                item.style.display = "none";
            }
        })
    })
    
    document.addEventListener('click', function(event) {
        var t = event.target;
        if (t.classList != 'main-select__main-item' && t.classList != 'main-select__main-item main-open' && t.classList != 'main-select__item-search' && t.classList != 'main-select__item--search') {
            document.querySelector('.main-select__other-item').classList.remove('open')
            document.querySelector('.main-select__main-item').classList.remove('main-open')
        }
    })
})