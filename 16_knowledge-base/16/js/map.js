window.addEventListener('DOMContentLoaded', function() {
    ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("map", {
            center: [48.87197540619385,2.3541857785611766],
            zoom: 15
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Собственный значок метки',
            balloonContent: 'Это метка из макета'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '../img/map.svg',
            iconImageSize: [28, 40],
            iconImageOffset: [-5, -38]
        
        })
        myMap.geoObjects
        .add(myPlacemark)
    }

        
})