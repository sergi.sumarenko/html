window.addEventListener('DOMContentLoaded', function() {
  const swiper = new Swiper('.swiper-container', {
    loop: true,

    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },

    scrollbar: {
      el: '.swiper-scrollbar',
    },
  });
})
