window.addEventListener('DOMContentLoaded', function() {
  document.querySelectorAll('.section-we__button').forEach(function(stepBtn) {
    stepBtn.addEventListener('click', function (event) {
      const path = event.currentTarget.dataset.path

      document.querySelectorAll('.we-list-item').forEach(function(stepContent) {
        stepContent.classList.remove('section-we__item--active')
      })

      document.querySelector(`[data-target="${path}"]`).classList.add('section-we__item--active')
    })
  })
})


