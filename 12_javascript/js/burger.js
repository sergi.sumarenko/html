window.addEventListener('DOMContentLoaded', function() {
  document.querySelector('.header-list__burger').addEventListener('click', function() {
    document.querySelector('.header__list').classList.toggle('header__list--open')
    document.querySelectorAll('.header-list__item').forEach(function(ev) {
      ev.classList.toggle('header-list__item--open')
    })
  })
  document.querySelectorAll('section').forEach(function (e) {
    e.addEventListener('click', function() {
      if (document.querySelector('.header__list--open') != null) {
        document.querySelector('.header__list').classList.remove('header__list--open')
        document.querySelectorAll('.header-list__item').forEach(function(ev) {
          ev.classList.remove('header-list__item--open')
        })
      }
    })
  })
})

