window.addEventListener('DOMContentLoaded', function() {
  $( function() {
    $( "#accordion" ).accordion({
      heightStyle: "content",
      icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-plus-act" },
      collapsible: true,
      active: false,
      animate: 650
    });
  });
})
